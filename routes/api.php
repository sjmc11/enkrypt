<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


$router->get('/', function () use ($router) {
    return response()->json(['status' => 'fail', 'message' => 'Denied'], 401);
});

$router->group(['prefix' => 'v1'], function () use ($router) {

    //These routes do not require tokens
    $router->post('auth/login', ['uses' => 'AuthController@login']);
    $router->post('auth/logout', ['uses' => 'AuthController@logout']);

    //These routes require tokens
    $router->group(['middleware' => 'enkrypt'], function () use ($router) {
        $router->get('me', ['uses' => 'AuthController@getTokenUserDetails']);
    });

});
