<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use App\AuthModel;

class TokenCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $auth = new AuthModel();

        if($auth->checkToken($request->header('enKrypt'))){
            return $next($request);
        }else{
            return response()->json(['status' => 'fail', 'message' => 'Token Expired or Invalid'], 401);
        }

    }
}
