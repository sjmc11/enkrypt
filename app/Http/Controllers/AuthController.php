<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AuthModel;

class AuthController extends Controller
{

    protected $authModel;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authModel = new AuthModel();
    }

    public function login (Request $request) {

        if($request->has('username') && $request->has('password') && $request->has('source')){

            if($uid = $this->authModel->checkLogin($request->get('username'), $request->get('password'))) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Authorisation Success, Token Supplied',
                    'token' => $this->authModel->generateToken($uid, $request->get('source')),
                    'enKrypt' => $request->header('enKrypt'),
                    'source' => $request->get('source')
                ], 200);

            }else{
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Invalid credentials supplied',
                    'enKrypt' => $request->header('enKrypt')
                ], 401);
            }

        }else{
            return response()->json([
                'status' => 'fail',
                'message' => 'Username and Password must be supplied',
                'enKrypt' => $request->header('enKrypt')
            ], 401);
        }

    }

    public function logout(Request $request) {
        $this->authModel->destroyToken($request->header('enKrypt'));

        return response()->json(['enKrypt' => $request->header('enKrypt')]);
    }

    public function getTokenUserDetails(Request $request) {
        $userData = $this->authModel->tokenUserDetails($request->header('enKrypt'));
        return response()->json($userData, 200);
    }
}
