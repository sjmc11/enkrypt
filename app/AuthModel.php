<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class AuthModel extends Model
{

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function checkLogin($username, $password)
    {
        $user = DB::table('users')->where('username', $username)->first();

        if (!empty($user)) {
            if (password_verify($password, $user->password)) {
                return $user->id;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function generateToken($userId, $source)
    {
        $user = DB::table('users')->where('id', $userId)->first();

        $token = md5(app('hash')->make($user->username . $user->password)) . '.' . md5(time() . str_random(32));

        $tokenRecord = [
            'token' => $token,
            'expires' => time() + 7200,
            'user_id' => $userId,
            'source' => $source
        ];

        //This is to allow only one login (per source) at a time
        DB::table('enkrypt_tokens')->where('user_id', $userId)->where('source', $source)->delete();

        DB::table('enkrypt_tokens')->insert($tokenRecord);

        return $token;
    }

    public function destroyToken($token)
    {
        DB::table('enkrypt_tokens')->where('token', $token)->delete();
    }

    public function checkToken($token)
    {
        $token = DB::table('enkrypt_tokens')->where('token', $token)->first();

        if (!empty($token)) {
            if ($token->expires > time()) {
                DB::table('enkrypt_tokens')->where('token', $token->token)->update(['expires' => time() + 7200]);
                return true;
            } else {
                $this->destroyToken($token->token);
                return false;
            }
        } else {
            return false;
        }
    }

    public function tokenUserDetails($token)
    {
        $token = DB::table('enkrypt_tokens')->select('expires', 'token', 'user_id')->where('token', $token)->first();
        $user = DB::table('users')->select('name', 'access_level')->where('id', $token->user_id)->first();

        return ['user' => $user, 'token' => $token];
    }
}